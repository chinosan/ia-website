import {logo} from '../assets';
const Hero = () => {
  return (
    <header>
        <nav>
            <img
            src={logo}
            />
            <button>
                gitlab
            </button>
        </nav>

        <h1>Sumrize articles with <br/>
        <span className='gradient-text' >OpenIA GPT-4</span>
        </h1>
        <h2>Simplify your reading with Summize, an open-source article summarizer
        that transforms lengthy articles into clear and concise summaries</h2>
    </header>
  )
}

export default Hero