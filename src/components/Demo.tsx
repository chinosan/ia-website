import {linkIcon } from '../assets'
const Demo = () => {
  return (
    <section>
        <div>
            <form>
                <img
                    src={linkIcon}
                />
                <input 
                type="url" 
                name="url" 
                id="" 
                placeholder='Paste the article link'
                />
                <button><p>↵</p></button>
            </form>

            {/* browse history */}
            <div>
                <p>URL</p>
            </div>

            {/* display result */}
            <div>
                
            </div>
        </div>
    </section>
  )
}

export default Demo